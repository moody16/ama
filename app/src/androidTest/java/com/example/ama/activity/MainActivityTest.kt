package com.example.ama.activity

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.example.ama.R
import org.junit.Rule
import org.junit.Test

class MainActivityTest {
    @JvmField
    @Rule
    val testRule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    @Test
    fun appLaunchesSuccessfully() {
        ActivityScenario.launch(MainActivity::class.java)

    }

    @Test
    fun initialViewsDisplayedProperly() {
        ActivityScenario.launch(MainActivity::class.java)
        onView(withId(R.id.btn_get_configuration)).check(matches(isDisplayed()))
        onView(withId(R.id.input_id_configuration)).check(matches(isDisplayed()))
        onView(withId(R.id.btn_get_configuration)).check(matches(isDisplayed()))

    }

    @Test
    fun whenGetButtonIsPressedAndConfigIsFilled() {

        // 1
        onView(withId(R.id.input_id_configuration)).perform(typeText("1"))

        onView(withId(R.id.btn_get_configuration)).perform(click())

    }
}