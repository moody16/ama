package com.example.ama.activity

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.example.ama.R
import org.junit.Rule
import org.junit.Test
import androidx.test.InstrumentationRegistry.getTargetContext
import android.os.Build
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Before
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import org.junit.After


class GeofenceActivityTest {

    @JvmField
    @Rule
    val testRule = ActivityTestRule<GeofenceActivity>(GeofenceActivity::class.java)

    @Before
    fun setUp() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getInstrumentation().uiAutomation.executeShellCommand(
                "pm revoke " + getTargetContext().packageName
                        + " android.permission.WRITE_EXTERNAL_STORAGE"
            )
        }
    }

    @Test
    fun initialViewsDisplayedProperly() {
        onView(withId(R.id.button_change_config)).check(matches(isDisplayed()))
        onView(withId(R.id.button_clear_logs)).check(matches(isDisplayed()))
        onView(withId(R.id.button_start)).check(matches(isDisplayed()))
        onView(withId(R.id.logs_textView)).check(matches(isDisplayed()))
        onView(withId(R.id.name_textView)).check(matches(isDisplayed()))
        onView(withId(R.id.configname_textView)).check(matches(isDisplayed()))
    }

    @After
    fun tearDown(){
        InstrumentationRegistry.getInstrumentation().uiAutomation.
            executeShellCommand("pm revoke ${getTargetContext().packageName} android.permission.WRITE_EXTERNAL_STORAGE")
    }
}