package com.example.ama.activity

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.ama.R
import com.example.ama.`interface`.APIService
import com.example.ama.`interface`.ReportRequestResultCallBack
import com.example.ama.model.ReportRequestResult
import com.example.ama.util.Constants
import com.example.ama.util.Constants.Companion.PERMISSION_ID
import com.example.ama.util.CustomHandler
import com.example.ama.util.SaveSharedPreference
import com.example.ama.util.TransitionIntentService
import com.google.android.gms.location.*
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_geofence.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@TargetApi(Build.VERSION_CODES.O)
class GeofenceActivity : AppCompatActivity(), CustomHandler.AppReceiver{

    lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val TAG = GeofenceActivity::class.java.simpleName

    var brodcastStatus: Boolean = false
    var latitude = 0.0
    var longitude = 0.0

    var accuracy = ""
    lateinit var mainHandler: Handler
    private lateinit var activityRecognitionClient: ActivityRecognitionClient
    private lateinit var transitionPendingIntent: PendingIntent
    val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")


    @TargetApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_geofence)
        initComponents()
        val current = LocalDateTime.now()
        val config = intent.getStringExtra("Config")

        //Transition API

        logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} $config"

        activityRecognitionClient = ActivityRecognition.getClient(this)

        val intent = Intent(this, TransitionIntentService::class.java)
        val handler = CustomHandler(this)
        intent.putExtra("handler", Messenger(handler))
        intent.putExtra("inputExtra","Foreground Service")
        transitionPendingIntent = PendingIntent.getService(this, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLastLocation()

        logs_textView.text = SaveSharedPreference.getLogs(applicationContext)!!

        mainHandler = Handler(Looper.getMainLooper())

    }

    private val updateTextTask = object : Runnable {
        val current = LocalDateTime.now()

        override fun run() {
            val interval = SaveSharedPreference.getIntervalInMinutes(applicationContext)
            print(interval)
            val reportRequestResult = ReportRequestResult(
                latitude = latitude,
                longitude = longitude,
                acc = accuracy,
                time = current.toString(),
                positionIntervalInMinutes = interval.toString(),
                tid = SaveSharedPreference.getObjectID(applicationContext).toString())

            sendLocation(reportRequestResult, object : ReportRequestResultCallBack {
                override fun onSuccess() {
                    Toast.makeText(
                        applicationContext,
                        "Location sent",
                        Toast.LENGTH_LONG
                    ).show()
                    logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Data sent"
                    SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Data sent")
                }

                override fun onFailure(errorMessage: String?) {
                    Toast.makeText(
                        applicationContext,
                        errorMessage,
                        Toast.LENGTH_SHORT
                    ).show()
                    logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} $errorMessage"
                }
            })

            mainHandler.postDelayed(this, interval!!.toLong() * 60000)
        }
    }

    override fun onPause() {
        super.onPause()
        mainHandler.removeCallbacks(updateTextTask)
    }

    override fun onResume() {
        super.onResume()
        mainHandler.post(updateTextTask)
    }

    fun registerHandler() {

        val test = activityRecognitionClient.requestActivityUpdates(100,transitionPendingIntent)
        Log.e(TAG, "Transition update set up" + test)
    }

    fun deregisterHandler() {
        activityRecognitionClient.removeActivityUpdates(transitionPendingIntent)
    }

    @TargetApi(Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n")
    private fun initComponents() {
        val token = SaveSharedPreference.getToken(applicationContext)!!
        val id = SaveSharedPreference.getObjectID(applicationContext)!!
        val interval = SaveSharedPreference.getIntervalInMinutes(applicationContext)!!
        val current = LocalDateTime.now()

        brodcastStatus = intent.getBooleanExtra("button", true)
        if (brodcastStatus) {
            button_start.text = "Stop"
        }else {
            button_start.text = "Start"
        }

        Log.e(TAG, "Token $token \n ObjectID $id \n Interval $interval ")
        button_start.setOnClickListener {

            if (!brodcastStatus) {
                button_start.text = "Stop"
                Log.e(TAG, "Start brodcasting")
                registerHandler()
                brodcastStatus = true
                mainHandler.post(updateTextTask)
            } else {
                button_start.text = "Start"
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Stoped"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Stoped")
                SaveSharedPreference.setDead(applicationContext, "no")
                Log.e(TAG, "Stop brodcasting")
                deregisterHandler()
                brodcastStatus = false
                mainHandler.removeCallbacks(updateTextTask)
            }
        }

        button_change_config.setOnClickListener {

            SaveSharedPreference.setDead(applicationContext, "no")
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)

        }

        button_clear_logs.setOnClickListener {
            logs_textView.text = ""
            SaveSharedPreference.clearLogs(applicationContext)
        }
    }

    private fun sendLocation(
        location: ReportRequestResult,
        callback: ReportRequestResultCallBack
    ) {
        val gson = GsonBuilder().setLenient().create()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        val serviceAPI = retrofit.create(APIService::class.java)
        val token = SaveSharedPreference.getToken(applicationContext)!!
        val authorization = "bearer $token"
        val call = serviceAPI.reportlocation(authorization, location)
        call.enqueue(object : Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                callback.onFailure("Connection failed!")
            }

            override fun onResponse(
                call: Call<Void>,
                response: Response<Void>
            ) {
                if (response.code() == 201) {
                    callback.onSuccess()
                } else {
                    callback.onFailure(response.errorBody()?.string())
                    print("")
                }
            }
        })
    }

    //Location API

    @TargetApi(Build.VERSION_CODES.O)
    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        val current = LocalDateTime.now()
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {

                        Log.e(
                            TAG,
                            "latitude: ${location.latitude}, longitude: ${location.longitude}"
                        )

                        latitude = location.latitude
                        longitude = location.longitude
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Turn on location"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Turn on location")
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            Log.e(TAG, "latitude: ${mLastLocation.latitude}, longitude: ${mLastLocation.longitude}")
            latitude = mLastLocation.latitude
            longitude = mLastLocation.longitude
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

    override fun onStop() {
        SaveSharedPreference.setDead(applicationContext, "yes")
        mainHandler.removeCallbacks(updateTextTask)
        super.onStop()
    }

    @TargetApi(Build.VERSION_CODES.O)
    override fun onReceiveResult(message: Message?) {
        val current = LocalDateTime.now()
        val transition = message!!.obj as DetectedActivity
        accuracy = transition.confidence.toString()
        when(transition.type.toString()){
            "0" -> {
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Transition: CAR"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Transition: CAR")
            }
            "1" -> {
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Transition: BIKE"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Transition: BIKE")

            }
            "2" -> {
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Transition: WALKING"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Transition: WALKING")

            }
            "3" -> {
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Transition: STILL"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Transition: STILL")

            }
            "4" -> {
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Transition: UNKNOWN"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Transition: UNKNOWN")

            }
            "7" -> {
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Transition: WALKING"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Transition: WALKING")
            }
            "8" -> {
                logs_textView.text = "${logs_textView.text} \n ${formatter.format(current)} Transition: RUNNING"
                SaveSharedPreference.setLogs(applicationContext, "${logs_textView.text} \n ${formatter.format(current)} Transition: RUNNING")
            }
        }
    }
}
