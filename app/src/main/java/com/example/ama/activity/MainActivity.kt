package com.example.ama.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.ama.R
import com.example.ama.`interface`.APIService
import com.example.ama.`interface`.GetConfigurationRequestResultCallBack
import com.example.ama.model.GetConfigurationRequestResult
import com.example.ama.util.Constants
import com.example.ama.util.SaveSharedPreference
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initComponents()

        val flag = SaveSharedPreference.getDead(applicationContext)

        if (flag == "yes") {
            val intent = Intent(applicationContext, GeofenceActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.putExtra("button", true)
            startActivity(intent)
        }
    }

    private fun initComponents() {
        btn_get_configuration.setOnClickListener {
            val configurationID =input_id_configuration.text.toString()

            downloadConfig(configurationID, object : GetConfigurationRequestResultCallBack {


                override fun onSuccess(result: GetConfigurationRequestResult) {
                    SaveSharedPreference.setToken(
                        applicationContext,
                        result.token,
                        result.trackedObjectId,
                        result.positionIntervalInMinutes
                    )
                    Toast.makeText(
                        applicationContext,
                        "Configuration downloaded successfully!",
                        Toast.LENGTH_SHORT
                    ).show()

                    val intent = Intent(applicationContext, GeofenceActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.putExtra("Config", "Configuration downloaded successfully!")
                    intent.putExtra("button", false)
                    startActivity(intent)
                }

                override fun onFailure(errorMessage: String?) {
                    Toast.makeText(
                        applicationContext,
                        "Bad request  $errorMessage",
                        Toast.LENGTH_LONG
                    ).show()

                }
            })
        }
    }

    private fun downloadConfig(id: String, callback: GetConfigurationRequestResultCallBack) {
        val gson = GsonBuilder().setLenient().create()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        val serviceAPI = retrofit.create(APIService::class.java)
        val call = serviceAPI.getConfiguration(id)
        call.enqueue(object : Callback<GetConfigurationRequestResult> {
            override fun onFailure(call: Call<GetConfigurationRequestResult>, t: Throwable) {
                callback.onFailure("Connection fail")
            }

            override fun onResponse(
                call: Call<GetConfigurationRequestResult>,
                response: Response<GetConfigurationRequestResult>
            ) {
                if (response.code() == 200) {
                    Log.d(
                        "RequestResult",
                        "Name: ${response.body()?.name} " +
                                "\nToken ${response.body()?.token}"+
                                "\nInterval ${response.body()?.positionIntervalInMinutes}"
                    )
                    response.body()?.let { callback.onSuccess(it) }
                } else {
                    callback.onFailure(response.errorBody()?.string())
                    print("")
                }
            }
        })
    }
}
