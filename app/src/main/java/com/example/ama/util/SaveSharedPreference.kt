package com.example.ama.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.ama.util.PreferencesUtility.KILLED
import com.example.ama.util.PreferencesUtility.LOGS
import com.example.ama.util.PreferencesUtility.POSITIONINTERVALINMINUTES
import com.example.ama.util.PreferencesUtility.TOKEN
import com.example.ama.util.PreferencesUtility.TRACKEDOBJECTID


object SaveSharedPreference {

    private fun getPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }


    fun setToken(
        context: Context, token: String?,
        trackedObjectID: String?,
        intervalInMinutes: String?
    ) {
        val editor = getPreferences(context).edit()
        editor.putString(TOKEN, token)
        editor.putString(TRACKEDOBJECTID, trackedObjectID)
        editor.putString(POSITIONINTERVALINMINUTES, intervalInMinutes)
        editor.apply()
    }

    fun setLogs(context: Context, logs: String){
        val editor = getPreferences(context).edit()
        editor.putString(LOGS, logs)
        editor.apply()
    }

    fun getLogs(context: Context): String?{
        return getPreferences(context).getString(LOGS,"logs")
    }

    fun clearLogs(context: Context){
        val editor = getPreferences(context).edit()
        editor.putString(LOGS, "")
        editor.apply()
    }

    fun setDead(context: Context, killed: String){
        val editor = getPreferences(context).edit()
        editor.putString(KILLED, killed)
        editor.apply()
    }

    fun getDead(context: Context): String{
        return getPreferences(context).getString(KILLED, null).toString()
    }

    fun getToken(context: Context): String? {
        return getPreferences(context).getString(TOKEN,"token")
    }
    fun getObjectID(context: Context): String? {
        return getPreferences(context).getString(TRACKEDOBJECTID,"trackedObjectId")
    }
    fun getIntervalInMinutes(context: Context): String? {
        return getPreferences(context).getString(POSITIONINTERVALINMINUTES,"positionIntervalInMinutes")
    }
}