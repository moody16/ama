package com.example.ama.util

class Constants {
    companion object {
        const val clientID = "mobile-client"
        const val clientSecretKey = "mobile@secret"
        const val baseUrl = "https://safe-plateau-50742.herokuapp.com/"
        const val PERMISSION_ID = 42
    }
}