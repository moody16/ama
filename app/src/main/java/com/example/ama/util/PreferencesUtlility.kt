package com.example.ama.util

object PreferencesUtility {

    const val TOKEN = "token"
    const val TRACKEDOBJECTID = "trackedObjectId"
    const val POSITIONINTERVALINMINUTES = "positionIntervalInMinutes"
    const val LOGS = "logs"
    const val KILLED = "killed"
}