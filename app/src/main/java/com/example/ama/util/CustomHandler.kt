package com.example.ama.util

import android.os.Handler
import android.os.Message

public class CustomHandler(receiver: AppReceiver?) : Handler() {
    private var appReceiver: AppReceiver? = receiver

    override fun handleMessage(msg: Message?) {
        super.handleMessage(msg)
        appReceiver!!.onReceiveResult(msg)
    }

    interface AppReceiver {
        fun onReceiveResult(message: Message?)
    }
}