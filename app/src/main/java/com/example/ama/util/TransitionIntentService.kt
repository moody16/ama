package com.example.ama.util

import android.app.*
import android.content.Intent
import android.os.Build
import android.os.Message
import android.os.Messenger
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.ama.activity.GeofenceActivity
import com.google.android.gms.location.ActivityRecognitionResult


class TransitionIntentService : IntentService("TransitionIntentService") {

    val CHANNEL_ID = "ForegroundServiceChannel"

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (ActivityRecognitionResult.hasResult(intent)) {
            val result = ActivityRecognitionResult.extractResult(intent)
            Log.i(TAG, "Activity Type "  + result.mostProbableActivity)
            val messenger: Messenger = intent!!.getParcelableExtra("handler")
            val msg = Message()
            msg.obj = result.mostProbableActivity
            messenger.send(msg)
        }

        val input = intent.getStringExtra("inputExtra")
        createNotificationChannel()

        val notificationIntent = Intent(this, GeofenceActivity::class.java)

        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Foreground Service")
            .setContentText(input)
            .setContentIntent(pendingIntent)
            .build()

        startForeground(1, notification)

        return Service.START_NOT_STICKY
    }

    override fun onHandleIntent(p0: Intent?) {
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(
                NotificationManager::class.java
            )
            manager.createNotificationChannel(serviceChannel)
        }
    }

    companion object {

        private val TAG = TransitionIntentService::class.java.simpleName
    }
}