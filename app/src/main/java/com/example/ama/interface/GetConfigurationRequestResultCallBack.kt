package com.example.ama.`interface`

import com.example.ama.model.GetConfigurationRequestResult


interface GetConfigurationRequestResultCallBack {
    fun onSuccess(result: GetConfigurationRequestResult)
    fun onFailure(errorMessage : String?)
}