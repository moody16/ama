package com.example.ama.`interface`


interface ReportRequestResultCallBack {
    fun onSuccess()
    fun onFailure(errorMessage : String?)
}