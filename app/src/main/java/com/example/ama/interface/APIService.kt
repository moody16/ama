package com.example.ama.`interface`

import com.example.ama.model.GetConfigurationRequestResult
import com.example.ama.model.ReportRequestResult
import retrofit2.Call
import retrofit2.http.*

interface APIService {


    @Headers("Accept: application/json")
    @GET("configs/{id}")
    fun getConfiguration(@Path("id") id: String): Call<GetConfigurationRequestResult>

    @Headers("Accept: application/json")
    @POST("motion_data")
    fun reportlocation(
        @Header("Authorization") authorizationKey: String,
        @Body location: ReportRequestResult
    ): Call<Void>

}