package com.example.ama.model

import com.google.gson.annotations.SerializedName

class ReportRequestResult(
    @SerializedName("accuracy") val acc: String = "acc",
    @SerializedName("altitude") val alt: Double = 0.0,
    @SerializedName("latitude") val latitude: Double = 0.0,
    @SerializedName("longitude") val longitude: Double = 0.0,
    @SerializedName("timestamp") val time: String = "",
    @SerializedName("trackedObjectId") val tid: String = "",
    @SerializedName("positionIntervalInMinutes") val positionIntervalInMinutes: String = "1"
)