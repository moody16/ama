package com.example.ama.model

import com.google.gson.annotations.SerializedName

class GetConfigurationRequestResult(
    @SerializedName("name") var name: String,
    @SerializedName("token") var token: String,
    @SerializedName("trackedObjectId") var trackedObjectId: String,
    @SerializedName("positionIntervalInMinutes") var positionIntervalInMinutes: String
)