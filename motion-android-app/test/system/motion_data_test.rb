require "application_system_test_case"

class MotionDataTest < ApplicationSystemTestCase
  setup do
    @motion_datum = motion_data(:one)
  end

  test "visiting the index" do
    visit motion_data_url
    assert_selector "h1", text: "Motion Data"
  end

  test "creating a Motion datum" do
    visit motion_data_url
    click_on "New Motion Datum"

    fill_in "Accuracy", with: @motion_datum.accuracy
    fill_in "Altitude", with: @motion_datum.altitude
    fill_in "Latitude", with: @motion_datum.latitude
    fill_in "Longitude", with: @motion_datum.longitude
    fill_in "Timestamp", with: @motion_datum.timestamp
    fill_in "Trackedobjectid", with: @motion_datum.trackedObjectId
    click_on "Create Motion datum"

    assert_text "Motion datum was successfully created"
    click_on "Back"
  end

  test "updating a Motion datum" do
    visit motion_data_url
    click_on "Edit", match: :first

    fill_in "Accuracy", with: @motion_datum.accuracy
    fill_in "Altitude", with: @motion_datum.altitude
    fill_in "Latitude", with: @motion_datum.latitude
    fill_in "Longitude", with: @motion_datum.longitude
    fill_in "Timestamp", with: @motion_datum.timestamp
    fill_in "Trackedobjectid", with: @motion_datum.trackedObjectId
    click_on "Update Motion datum"

    assert_text "Motion datum was successfully updated"
    click_on "Back"
  end

  test "destroying a Motion datum" do
    visit motion_data_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Motion datum was successfully destroyed"
  end
end
