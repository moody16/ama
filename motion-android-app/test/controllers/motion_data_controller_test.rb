require 'test_helper'

class MotionDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @motion_datum = motion_data(:one)
  end

  test "should get index" do
    get motion_data_url
    assert_response :success
  end

  test "should get new" do
    get new_motion_datum_url
    assert_response :success
  end

  test "should create motion_datum" do
    assert_difference('MotionDatum.count') do
      post motion_data_url, params: { motion_datum: { accuracy: @motion_datum.accuracy, altitude: @motion_datum.altitude, latitude: @motion_datum.latitude, longitude: @motion_datum.longitude, timestamp: @motion_datum.timestamp, trackedObjectId: @motion_datum.trackedObjectId } }
    end

    assert_redirected_to motion_datum_url(MotionDatum.last)
  end

  test "should show motion_datum" do
    get motion_datum_url(@motion_datum)
    assert_response :success
  end

  test "should get edit" do
    get edit_motion_datum_url(@motion_datum)
    assert_response :success
  end

  test "should update motion_datum" do
    patch motion_datum_url(@motion_datum), params: { motion_datum: { accuracy: @motion_datum.accuracy, altitude: @motion_datum.altitude, latitude: @motion_datum.latitude, longitude: @motion_datum.longitude, timestamp: @motion_datum.timestamp, trackedObjectId: @motion_datum.trackedObjectId } }
    assert_redirected_to motion_datum_url(@motion_datum)
  end

  test "should destroy motion_datum" do
    assert_difference('MotionDatum.count', -1) do
      delete motion_datum_url(@motion_datum)
    end

    assert_redirected_to motion_data_url
  end
end
