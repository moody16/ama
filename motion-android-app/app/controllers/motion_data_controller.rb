class MotionDataController < ApplicationController
  before_action :set_motion_datum, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token

  # GET /motion_data
  # GET /motion_data.json
  def index
    @motion_data = MotionDatum.all
  end

  # GET /motion_data/1
  # GET /motion_data/1.json
  def show
  end

  # GET /motion_data/new
  def new
    @motion_datum = MotionDatum.new
  end

  # GET /motion_data/1/edit
  def edit
  end

  # POST /motion_data
  # POST /motion_data.json
  def create
    @motion_datum = MotionDatum.new(motion_datum_params)
    hmac_secret = 'my$ecretK3y'

    token = request.headers["Authorization"]
    token = token.split(" ")[1]
    #byebug
    begin
      decoded_token = JWT.decode token, hmac_secret, true, { algorithm: 'HS256' }
      respond_to do |format|
        if @motion_datum.save
          format.html { redirect_to @motion_datum, notice: 'Motion datum was successfully created.' }
          format.json { render :show, status: :created, location: @motion_datum }
        else
          format.html { render :new }
          format.json { render json: @motion_datum.errors, status: :unprocessable_entity }
        end
      end

    rescue JWT::ExpiredSignature
      puts "Token expired"
    end
  end

  # PATCH/PUT /motion_data/1
  # PATCH/PUT /motion_data/1.json
  def update
    respond_to do |format|
      if @motion_datum.update(motion_datum_params)
        format.html { redirect_to @motion_datum, notice: 'Motion datum was successfully updated.' }
        format.json { render :show, status: :ok, location: @motion_datum }
      else
        format.html { render :edit }
        format.json { render json: @motion_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /motion_data/1
  # DELETE /motion_data/1.json
  def destroy
    @motion_datum.destroy
    respond_to do |format|
      format.html { redirect_to motion_data_url, notice: 'Motion datum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_motion_datum
      @motion_datum = MotionDatum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def motion_datum_params
      params.permit(:accuracy, :altitude, :longitude, :timestamp, :trackedObjectId, :latitude)
    end
end
