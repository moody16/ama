json.extract! motion_datum, :id, :accuracy, :altitude, :longitude, :timestamp, :trackedObjectId, :latitude, :created_at, :updated_at
json.url motion_datum_url(motion_datum, format: :json)
