json.extract! config, :id, :positionIntervalInMinutes, :token, :trackedObjectId, :created_at, :updated_at
json.url config_url(config, format: :json)
