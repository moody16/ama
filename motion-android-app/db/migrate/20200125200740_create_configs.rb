class CreateConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :configs do |t|
      t.string :positionIntervalInMinutes
      t.string :token
      t.string :trackedObjectId

      t.timestamps
    end
  end
end
