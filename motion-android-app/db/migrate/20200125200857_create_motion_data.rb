class CreateMotionData < ActiveRecord::Migration[6.0]
  def change
    create_table :motion_data do |t|
      t.string :accuracy
      t.string :altitude
      t.string :longitude
      t.string :timestamp
      t.string :trackedObjectId
      t.string :latitude

      t.timestamps
    end
  end
end
