Rails.application.routes.draw do
  resources :motion_data
  resources :configs
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
